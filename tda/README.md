# Topological Data Analysis - docker image

## Usage:

* On linux:
  * `docker run -d -v $(pwd):/work jandot/tda`
* On Mac:
  * `socat TCP-LISTEN:6000,reuseaddr,fork UNIX-CLIENT:\"$DISPLAY\"`
  * Start XQuartz
  * `docker run -d -v $(pwd):/work -e DISPLAY=192.168.99.1:0 jandot/tda`

Note: The IP number used here should not be the one reported with `docker-machine ip default` (often 192.168.99.100), but the one that is reported under `vboxnet0` when running `ifconfig`.

## Links:

* [Python mapper](http://danifold.net/mapper/installation/index.html)
* [How to run GUI from docker](http://kartoza.com/how-to-run-a-linux-gui-application-on-osx-using-docker/)
