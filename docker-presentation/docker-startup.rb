#!/usr/bin/env ruby
altered_index = File.read("/opt/presentation/index.html_original")
File.open("/opt/presentation/slides/config.tsv").each do |line|
  k,v = line.chomp.split(/\t/)
  if k == "CONF_TITLE"
    altered_index.sub!(/CONF_TITLE/, v)
  elsif k == "CONF_AUTHOR"
    altered_index.sub!(/CONF_AUTHOR/, v)
  elsif k == "CONF_DESCRIPTION"
    altered_index.sub!(/CONF_DESCRIPTION/, v)
  elsif k == "CONF_PROGRESS"
    if v == "TRUE"
      altered_index.sub!(/CONF_PROGRESS/,",{ src: 'plugin/toc-progress/toc-progress.js', async: true, callback: function() { toc_progress.initialize(); toc_progress.create(); } }")
    end
  end
end
altered_index.sub!(/CONF_PROGRESS/,'')
altered_index.sub!(/simple/,'jandot')
altered_index.sub!(/Simple/,'Jandot')

altered_gruntfile = File.read("/opt/presentation/Gruntfile.js_original")
altered_gruntfile.sub!(/root.map.*html.*/, '[ "index.html" ],')
altered_gruntfile.sub!(/root.map.*md.*/, '[ "slides/slides.md" ],')

script_paths = []
Dir["/opt/presentation/scripts/*"].each do |f|
  f.sub!(/\/opt\/presentation\//,'')
  altered_index.sub!(/CONF_SCRIPTS/, ['CONF_SCRIPTS','<script type="text/javascript" src="' + f + '"></script>'].join("\n"))
end
altered_index.sub!(/CONF_SCRIPTS/,'')

File.open("/opt/presentation/index.html", "w") {|file| file.puts altered_index }
File.open("/opt/presentation/Gruntfile.js", "w") {|file| file.puts altered_gruntfile }

exec("grunt serve")
