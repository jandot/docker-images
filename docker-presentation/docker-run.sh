docker run \
-p 8000:8000 -p 35729:35729 \
--name docker_presentation \
-v /Users/jaerts/Projects/docker-images/docker-presentation/images:/opt/presentation/images \
-v /Users/jaerts/Projects/docker-images/docker-presentation/slides:/opt/presentation/slides \
-v /Users/jaerts/Projects/docker-images/docker-presentation/scripts:/opt/presentation/scripts \
-d jandot/docker-presentation
