# Docker image for presentations
Docker image containing reveal.js, based on `jamtur01/docker-presentation` by James Turnbull.

## How to use this image?
* Create a `slides/` and `images/` directory.
* In the `slides/` directory: create a new file `slides.md` which will contain your full presentation.
* To switch to the next horizontal slide, use `--NEWH`; for the next vertical slide, use `--NEWV`.
* Run the following command:

```
docker run \
  -p 8000:8000 -p 35729:35729 \
  --name docker_presentation \
  -v /path/to/workingdirectory/images:/opt/presentation/images \
  -v /path/to/workingdirectory/slides:/opt/presentation/slides \
  -d jandot/docker-presentation
```

* Open `http://localhost:8000`. If necessary, use the docker IP address instead of `localhost`.

## Adding visualizations
This docker image automatically picks up any `.js` file in the `scripts` directory. To include visualizations, do the following:

* Put the javascript code itself into the scripts file (1 script per visualization). An example code block:
```javascript
var viz_function = function(p) {
  p.setup = function() {
    var myCanvas = p.createCanvas(100,100)
    myCanvas.parent('viz1')
    p.noStroke()
    p.noLoop()
  }

  p.draw = function() {
    p.background(255,255,255)
    p.fill(0,255,0)
    p.ellipse(p.mouseX,50,20,20)
  }

	p.mouseMoved = function() {
		p.redraw()
	}
}
var viz = new p5(viz_function)
```
* In your `slides.md`, add the `div` where the visualization should be inserted.
```pre
## My slide
Here's an interactive visualization
<div id="viz1"></div>
```

The `id` of this `div` should be the same as the one mentioned in `myCanvas.parent()` in the script. It should be unique.

## The footer
I've added a footer to the slides to show the progress. See [TOC-progress](http://e-gor.github.io/Reveal.js-TOC-Progress/) for more info and customization. To know how titles are selected for this footer, see the [Presentable plugin](http://fcfeibel.com/presentable).

Basically, each new horizontal slide becomes a new entry in the left column. Any h1, h2 or h3 headers in that section of vertical slides become entries in the right column.

* To hide/show the footer, use `q`.
* To remove a header from the footer (e.g. the title of your presentation), you'll have to use plain html instead of markdown, because you have to add a class to it. For example: use `<h2 class="no-toc-progress">My title</h2>` instead of `## My title`.
