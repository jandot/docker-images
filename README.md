# Now at ESAT gitlab

# Collection of docker images
This repository contains a collection of docker images, primarily meant to support the course "I0U19A - Management of Large-Scale Omics Data" at KU Leuven.

First run the i0u19a-data container, as the other ones will try to sshfs-mount the data directory!