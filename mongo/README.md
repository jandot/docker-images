# Mongo docker container
## Building and running
```
sh docker-build.sh
sh docker-run.sh
```

### Issues
`/data/db` from the host cannot be mounted into the container unless you're working on a linux box => still need to find another solution to allow persistence of the data

## Testing
If the container is running, check if you can get in with ssh: `ssh -p 30022 root@192.168.99.100`. Also, you should be able to connect to the mongo server: `mongo --host 192.168.99.100`.
