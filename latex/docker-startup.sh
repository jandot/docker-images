#!/bin/sh
inputfile=$1
basename=`echo $inputfile | sed -e "s/.tex//"`

cd /source
latex $basename
bibtex $basename
latex $basename
latex $basename
dvips -t a4 -Ppdf -G0 -o $basename.ps $basename.dvi
ps2pdf -dMaxSubsetPct=100 \
       -dCompatibilityLevel=1.3 \
       -dSubsetFonts=true \
       -dAutoFilterColorImages=false \
       -dAutoFilterGrayImages=false \
       -dColorImageFilter=/FlateEncode \
       -dGrayImageFilter=/FlateEncode \
       $basename.ps \
       $basename.pdf
