#!/bin/sh
# Adapted from https://gist.github.com/jexp/4a9dfd20cff6e270ea92dd3397ffb4ba
export NEO4J_HOME=/var/lib/neo4j

if [ ! -f data-csv.zip ]; then
  echo  Downloading ...

  curl -OL https://cloudfront-files-1.publicintegrity.org/offshoreleaks/data-csv.zip
fi

# Need full path due to BUG in import tool with relative paths	
export DATA=${PWD}/import

echo Extracting, Preparing, Cleaning up data ...

unzip -o -j data-csv.zip -d $DATA

tr -d '\\' < $DATA/Addresses.csv > $DATA/Addresses_fixed.csv

sed -i.bak -e '1,1 s/node_id/node_id:ID(Address)/'      $DATA/Addresses_fixed.csv
sed -i.bak -e '1,1 s/node_id/node_id:ID(Officer)/'      $DATA/Officers.csv
sed -i.bak -e '1,1 s/node_id/node_id:ID(Entity)/'       $DATA/Entities.csv
sed -i.bak -e '1,1 s/node_id/node_id:ID(Intermediary)/' $DATA/Intermediaries.csv

sed -i.bak -e '1 d' $DATA/all_edges.csv

for i in Entity Officer Intermediary; do 
   echo "node_id:START_ID($i),detail:IGNORE,node_id:END_ID(Address)" > $DATA/registered_address_$i.csv
done
grep ',registered address,' $DATA/all_edges.csv > $DATA/registered_address.csv

for i in Officer Intermediary; do 
for j in Officer Intermediary; do 
   echo "node_id:START_ID(${i}),detail:IGNORE,node_id:END_ID(${j})" > $DATA/similar_${i}_${j}.csv
done
done
grep ',similar name and address as,' $DATA/all_edges.csv > $DATA/similar.csv

echo 'node_id:START_ID(Entity),detail,node_id:END_ID(Entity)' > $DATA/related.csv
grep ',\(related entity\|same name and registration date as\),' $DATA/all_edges.csv >> $DATA/related.csv


for i in Entity Intermediary; do 
   echo "node_id:START_ID(Officer),detail,node_id:END_ID($i)" > $DATA/officer_of_$i.csv
done
tr '[:upper:]' '[:lower:]' < $DATA/all_edges.csv | grep -v ',\(intermediary of\|registered address\|similar name and address as\|same name and registration date as\|same address as\|related entity\),'  > $DATA/officer_of.csv

for i in Entity; do 
   echo "node_id:START_ID(Intermediary),detail,node_id:END_ID($i)" > $DATA/intermediary_of_$i.csv
done
sed -e 's/,intermediary of,/,,/' < $DATA/all_edges.csv > $DATA/intermediary_of.csv


echo  CSV Overview ...

head -1 $DATA/*.csv

echo  Importing ...


rm -rf $NEO4J_HOME/data/graph.db; $NEO4J_HOME/bin/neo4j-import --into $NEO4J_HOME/data/graph.db --nodes:Address $DATA/Addresses_fixed.csv --nodes:Entity $DATA/Entities.csv --nodes:Intermediary $DATA/Intermediaries.csv --nodes:Officer $DATA/Officers.csv \
           --relationships:REGISTERED_ADDRESS $DATA/registered_address_Officer.csv,$DATA/registered_address.csv \
           --relationships:REGISTERED_ADDRESS $DATA/registered_address_Entity.csv,$DATA/registered_address.csv \
           --relationships:REGISTERED_ADDRESS $DATA/registered_address_Intermediary.csv,$DATA/registered_address.csv \
           --relationships:RELATED $DATA/related.csv \
           --relationships:OFFICER_OF $DATA/officer_of_Entity.csv,$DATA/officer_of.csv \
           --relationships:OFFICER_OF $DATA/officer_of_Intermediary.csv,$DATA/officer_of.csv \
           --relationships:INTERMEDIARY_OF $DATA/intermediary_of_Entity.csv,$DATA/intermediary_of.csv \
           --relationships:SIMILAR $DATA/similar_Officer_Officer.csv,$DATA/similar.csv \
           --relationships:SIMILAR $DATA/similar_Officer_Intermediary.csv,$DATA/similar.csv \
           --relationships:SIMILAR $DATA/similar_Intermediary_Officer.csv,$DATA/similar.csv \
           --relationships:SIMILAR $DATA/similar_Intermediary_Intermediary.csv,$DATA/similar.csv \
           --ignore-empty-strings true --skip-duplicate-nodes true --skip-bad-relationships true --bad-tolerance  10000000 --multiline-fields=true     

echo  Import finished ...
