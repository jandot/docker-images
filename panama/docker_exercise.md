---
title: Docker exercise with Panama papers
author: Jan Aerts
date: May 14, 2016
---
**See http://neo4j.com/blog/analyzing-panama-papers-neo4j/**


In this session, we'll create a docker image with neo4j installed with the Panama papers data loaded. We will not look into all the ins-and-outs of docker images (such as volumes), but walk through the process of creating one. The online [docker documentation](https://docs.docker.com/) is quite extensive and provides some good walkthroughs.

# Creating docker images

## Terminology
* *docker image*: an immutable description of a system
* *docker container*: an running version of a docker image
* *docker hub*: central repository of docker images

## Docker hub

![docker-hub](/source/images/docker-hub.png)

Let's first create an account on Docker hub [http://hub.docker.com](here). As you'll be working on your image, you'll push it to that hub so that it becomes available later both for yourself and for other people.

## The Dockerfile
The best way to create a docker image is through a *Dockerfile* (which is actually a file called `Dockerfile` (with capital "D")). This file is a description of what is contained in the image.

A Dockerfile must start with a *base image*. This could be a bare ubuntu, or you can start from a system that for example already has mysql (or neo4j, or anything else) installed.

## Minimal Dockerfile
Let's create a minimal Dockerfile. In a new directory, create a file called `Dockerfile`. Write just a single line in it:
```
FROM ubuntu
```
and save. You now have an image description that can be "built" into an image.

## Building the image
To build an image based on a Dockerfile, issue the following command: `docker build --rm -t <your-id>/my-docker .`, where `<your-id>` is the ID you have on docker hub. Don't forget the period at the end of the command (which just means that `docker build` will search for the Dockerfile in the current directory).

When you've issued this command, you should see the image listed in your installed images with `docker images`.

## Running the image
To see if this actually worked, let's run the image: `docker run -it --rm <your-id>/my-docker /bin/bash`.

You're now running a clean ubuntu system! You should be able to run any linux command here, although many of these might have to be installed still: it's a *minimal* ubuntu installation. For example, `wget` is not installed. To install that, run `apt-get install wget`. No need to `sudo`, as you're logged in as root.

To exit the ubuntu installation, type `exit` as if you would log out of a regular linux system.

Let's go over the command we typed:

* `docker run`: run the image as a container, obviously
* `-it`: run as an interactive session
* `--rm`: remove the container when you exit
* `<your-id>/my-docker`: which image to run
* `/bin/bash`: which command to run

Note that the image itself is immutable: if you run that image, installed software, and exit, then that installed software is gone. Rerunning the `docker run` command you will start with a clean slate again. This has the clear advantage that you can just exit a container and start it up again if you messed up; the disadvantage of course that you have to reinstall your software every single time.

But that's something that the Dockerfile can do for us.

## Specifying commands in Dockerfile
Instead of running the container and installing `wget` in there, we can already build `wget` into the image itself. We do that by just adding the installation command in the Dockerfile, preceded by the keyword `RUN` (in capitals).

Above - while being in our running ubuntu container - we installed `wget` using `apt-get install wget`. So what we need to add to the Dockerfile, is
```
RUN apt-get update
RUN apt-get install wget
```

**To check: why do we need the apt-get update?**

However, rebuilding this image will not work. The reason being that commands specified in the Dockerfile cannot rely on user input. This `apt-get` command, however, will ask at a certain point if all dependencies need to be installed. We can let it do that automatically by adding the `-y` flag. So instead of the above line in the Dockerfile, use
```
RUN apt-get update
RUN apt-get install -y wget
```

So at this moment, your Dockerfile will look like this:
```
FROM ubuntu
RUN apt-get update
RUN apt-get install -y wget
```

Now rebuild your image with `docker build --rm -t <your-id>/my-docker .` Running it (`docker run -it --rm <your-id>/my-docker /bin/bash`) you will now notice that the `wget` command is available from the start.

## The default command
You now always have to add `/bin/bash` at the end of your `docker run` command because the image does not know what to return: should it start a service, run a particular command, or return a shell? That's what the last option on the command line (the `/bin/bash`) does. But you can actually define a default command in the Dockerfile. That's what the `CMD` command does:

```
CMD ["/bin/bash"]
```

Caution: this should always be the last line in the Dockerfile! The `CMD` line takes an array. For reasons that we won't go into here, the command that you specify there with its arguments should be separate elements in that array. For example, if you want to run `ls -lrt`, you have to specify that as

```
CMD ["ls", "-lrt"]
```

This `CMD` line will only be relevant if you don't specify a command as the last argument in your `docker run`. So even if you have a `CMD ["/bin/bash"]` in your Dockerfile, you can still do `docker run -it --rm <your-id>/docker-exercise ls /data`. This will *not* run `/bin/bash`, but just show the contents of the `/data` directory in the image and return back to your prompt.

## Copying files into the container
Above, we saw how to install software in a bare container. For some purposes, we will also want to add some data or files into the container itself. Here, we'll use two methods of getting the Panama paper data (available from the [ICIJ website](https://offshoreleaks.icij.org/pages/database)).

### Getting data that can be reached over the network
One way of adding these data to the image, is to download them during the build process.

Add the following lines to your Dockerfile:
```
RUN apt-get install -y zip
RUN mkdir -p /data/panama-papers
WORKDIR /data/panama-papers
RUN wget https://cloudfront-files-1.publicintegrity.org/offshoreleaks/data-csv.zip
RUN unzip -xzf data-csv.zip
RUN rm data-csv.zip

CMD ["/bin/bash"]
```

This will create a new directory `/data/panama-papers`, `cd` into that directory with `WORKDIR`, download the data, and then extract it. We remove the downloaded zip-file itself afterwards, because we don't need it anymore after it's extracted.

Your Dockerfile will now look like this:
```
FROM ubuntu

RUN apt-get update
RUN apt-get install -y wget
RUN apt-get install -y zip
RUN mkdir -p /data/panama-papers
WORKDIR /data/panama-papers
RUN wget https://cloudfront-files-1.publicintegrity.org/offshoreleaks/data-csv.zip
RUN unzip data-csv.zip
RUN rm data-csv.zip

CMD ["/bin/bash"]
```

Let's check if this worked. Start the container as above. An `ls /` should show that there is a `/data` directory. Going into it, you'll find the Panama papers network data: `/data/panama-papers`.

#### Intermezzo: debugging your Dockerfile
You might find that your build command will return an error at some point. Luckily, docker works in such way that (certain types of) debugging is simple. Each line in the Dockerfile will actually create a separate image. The name that you define in the build command is just the name that will be given after the last command in the file has run.

Running the build command, you'll get some output similar to this:
```
Sending build context to Docker daemon 2.048 kB
Step 1 : FROM ubuntu
 ---> 44776f55294a
Step 2 : RUN apt-get update
 ---> Running in 3ce96d045c4a
Get:1 http://archive.ubuntu.com/ubuntu xenial...
Get:2 http://archive.ubuntu.com/ubuntu xenial-updates...
...
Get:21 http://archive.ubuntu.com/ubuntu xenial-security...
Fetched 23.0 MB in 15s (1500 kB/s)
Reading package lists...
 ---> 17a12254d183
Removing intermediate container 3ce96d045c4a
Step 3 : RUN apt-get install -y wget
 ---> Running in 4d505cdd3134
Reading package lists...
Building dependency tree...
The following additional packages will be installed:
  ca-certificates libidn11 libssl1.0.0 openssl
The following NEW packages will be installed:
  ca-certificates libidn11 libssl1.0.0 openssl wget
0 upgraded, 5 newly installed, 0 to remove and 5 not upgraded.
...
173 added, 0 removed; done.
Running hooks in /etc/ca-certificates/update.d...
done.
 ---> 971a18ed6ca3
Removing intermediate container 4d505cdd3134
Step 4 : RUN apt-get install -y zip
 ---> Running in 27899fb3d4e9
Reading package lists...
Building dependency tree...
Reading state information...
...
```

Note the `---> <some-id>` lines. These are the IDs of intermediate images. The image "44776f55294a" is the base ubuntu image; the one called "971a18ed6ca3" has `wget` installed, but not `zip` yet. You can run any of these intermediate images as well. Suppse that you see an error in the part that installs the `zip` package. You could run the "971a18ed6ca3" image, and try to install `zip` manually by typing in the command that is in the Dockerfile, and debug.

```
docker run -it --rm <your-id>/my-docker /bin/bash
```

### Copying the files from our local drive
A second way of making the Panama papers available in the image, is by first downloading them onto our own computer, and copy them into the image.

Download the `data-csv.zip` file in the same directory as the Dockerfile. Then adjust your Dockerfile to look like this:

```
FROM ubuntu

RUN apt-get update
RUN apt-get install -y zip
RUN mkdir -p /data/panama-papers
WORKDIR /data/panama-papers
ADD data-csv.zip
RUN unzip data-csv.zip
RUN rm data-csv.zip

CMD ["/bin/bash"]
```

The `ADD` command takes a local file at build time and copies it into the image. This might be the approach to use if the file to copy is not available through a public URL.

## Some best practices for the Dockerfile
* Add maintainer information on the line below the `FROM` command: `MAINTAINER your name <your@email.address>`.
* Specify version number for the base image and all installed packages.

Given these 2 tips, your final Dockerfile should look like this:
```
FROM ubuntu:16.04
MAINTAINER Your Name <your-email-address>

RUN apt-get update
RUN apt-get install -y wget=1.17.1-1ubuntu1
RUN apt-get install -y zip=3.0-11
RUN mkdir -p /data/panama-papers
WORKDIR /data/panama-papers
RUN wget https://cloudfront-files-1.publicintegrity.org/offshoreleaks/data-csv.zip
RUN unzip data-csv.zip
RUN rm data-csv.zip

CMD ["/bin/bash"]
```

## Pushing your image to docker hub
Once you have a version of your image that you're happy with, you can push it to docker hub for others to use as well. The first time you do this, you'll have to log into docker hub from the command line with `docker login`. Once you've done that, you can push using `docker push <your-id>/docker-exercise`.

From this point onwards, anyone can run your image using the command `docker run -it --rm <your-id>/docker-exercise /bin/bash`.

# Back to the Panama papers: preparing
But we want to investigate the Panama papers. The website of the [International Consortium of Investigative Journalism](https://offshoreleaks.icij.org/pages/database) mentions that the data is available as a [Neo4j](http://neo4j.com) graph database. So to investigate the data, we will want to install the Neo4j database system. We could do this natively onto our own machine, but let's do this using docker.

## What does the data look like?
The zip file that we downloaded contains 5 files:

* Addresses.csv
    * address
    * icij_id
    * valid_until
    * country_codes
    * countries
    * node_id
    * sourceID
* Entities.csv
    * name
    * original_name
    * former_name
    * jurisdiction
    * jurisdiction_description
    * company_type
    * address
    * internal_id
    * incorporation_date
    * inactivation_date
    * struck_off_date
    * dorm_date
    * status
    * service_provider
    * ibcRUC
    * country_codes
    * countries
    * note
    * valid_until
    * node_id
    * sourceID
* Intermediaries.csv
    * name
    * internal_id
    * address
    * valid_until
    * country_codes
    * countries
    * status
    * node_id
    * sourceID
* Officers.csv
    * name
    * icij_id
    * valid_until
    * country_codes
    * countries
    * node_id
    * sourceID
* all_edges.csv
    * node_1
    * rel_type
    * node_2

All these file contain node information, except `all_edges.csv` which are the relationships.

The [offschoreleaks](https://offshoreleaks.icij.org/pages/about) website provides some more information on what these terms mean exactly:

* *Entity* - A company, trust or fund created in a low-tax, offshore jurisdiction by an agent
* *Agent* - Firm that provides services in an offshore jurisdiction to incorporate, register and manage an offshore entity at the request of a client
* *Officer* - A person or company who plays a role in an offshore entity
* *Intermediary* - A go-between for someone seeking an offshore corporation and an offshore service provider -- usually a law-firm or a middleman that asks an offshore service provider to create an offshore firm for a client

## The panama docker image
To create a docker image to investigate this data, we will have to do the following:

* Install neo4j
* Copy the data onto the image
* Import it with `neo4j-import`
* Start the neo4j server

We will do the first 2 steps in the Dockerfile itself; the last two steps will be run through a script that we will as the `CMD` from that Dockerfile.

### Install neo4j
Given what we learned above, we could start from an ubuntu base image, and install neo4j. Of course, that is a viable option. But interestingly, there is a neo4j base image as well.

```
FROM neo4j:2.3
MAINTAINER Your Name <your email>
```

To get everything working as we want to, you'll also have to add the following lines to the Dockerfile:
```
RUN sed -i.bak 's/dbms.security.auth_enabled=true/dbms.security.auth_enabled=false/' /var/lib/neo4j/conf/neo4j-server.properties
RUN echo "node_auto_indexing=true" >> /var/lib/neo4j/conf/neo4j.properties
RUN echo "node_keys_indexable=name" >> /var/lib/neo4j/conf/neo4j.properties
```
The problem is that neo4j by default requires the user to reset the default username and password at first use. But that's interactive, which we must avoid. The above lines take care of that. In addition, it configures neo4j to automatically indexes the `name` property of all nodes.

#### Copy the data onto the image
As we saw above, we can either download the files locally and `ADD` them to the image, or we can `wget` them directly in the image. Let's go for the second option.

```
RUN apt-get install -y zip=3.0-8
RUN mkdir /startup
WORKDIR /startup
RUN wget https://cloudfront-files-1.publicintegrity.org/offshoreleaks/data-csv.zip
RUN unzip data-csv.zip
RUN rm data-csv.zip
```

## Loading the data

# The Panama papers: questions to ask
**to do**

----

The Dockerfile

Create account on docker hub

Create Dockerfile

Docker build
-> iterate over steps

Install neo4j


Load data


Analyze data
